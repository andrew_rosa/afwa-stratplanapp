# ds_411
# 
# This Dashboard is to show data for Goal 4 Measure 1, in relation to State and
# Wildlife grants that AFWA helps to administer. 
# 

# Define UI for dashboard ------------------------------------------------------

ds_411_UI <- function(id) {
  temp <- tagList(
    tags$head(
      tags$link(rel="stylesheet", type="text/css", href="dash.css")),
    div(style = "margin: 15px",
      div(style = "float:left",
          uiOutput(NS(id, "tstitle"), class = "dashtitls"),
          girafeOutput(NS(id, "timeseries"), height = "405px", width = "485px")
      ),
      div(style = "float:right",
          uiOutput(NS(id, "cmtitle"), class = "dashtitls"),
          leafletOutput(NS(id, "map"), width = "485px", height = "404px")
      ),
      uiOutput(NS(id, "debug"), style = "pointer-events: none;")
    )
  )
  
  return(temp)
}

# Define server module logic ---------------------------------------------------

ds_411_Server <- function(id) {
  moduleServer(id, function(input, output, session) {
    
    # Plots to generate
    output$tstitle <- renderUI({
      pl_ds411_timeseries_title()
    })
    
    output$timeseries <- renderGirafe({
      pl_ds411_timeseries()
    })
    
    output$cmtitle <- renderUI({
      pl_ds411_choroplethmap_title(input$timeseries_selected)
    })
    
    output$map <- renderLeaflet({
      pl_ds411_choroplethmap(input$timeseries_selected)
    })
    
    # Tooltips for plots
    # output$debug <- renderUI({
    # xvar <- input$timeseries_selected
    # p(HTML(paste0("<b>", xvar, ": </b>")))
    # })
    
  })
}

