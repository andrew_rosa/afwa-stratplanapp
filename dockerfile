# Base image https://hub.docker.com/u/rocker/
FROM rocker/shiny:latest

# system libraries of general use
## install debian packages
RUN apt-get update -y && apt-get -y install libgdal-dev

RUN apt-get update -qq && apt-get -y --no-install-recommends install \
    libxml2-dev \
    libcairo2-dev \
    libsqlite3-dev \
    libpq-dev \
    libssh2-1-dev \
    unixodbc-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    libudunits2-dev \
    libgeos-dev \
    postgis

## update system libraries
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean
    
# copy the app to the image
COPY app /srv/shiny-server/afwaapp
## renv.lock file to install R packages
COPY  renv.lock /srv/shiny-server/

# add new shiny server config
COPY shiny-server.conf /etc/shiny-server/

# allow permission
RUN sudo chown -R shiny:shiny /usr/local/lib/R/site-library

# install renv & restore packages
RUN Rscript -e 'install.packages("renv")'
RUN Rscript -e 'renv::consent(provided = TRUE)'
RUN sudo -u shiny Rscript -e 'renv::restore("/srv/shiny-server")'

RUN echo '.libPaths("/srv/shiny-server/renv/library/R-3.6/x86_64-pc-linux-gnu")' >> /usr/local/lib/R/etc/Rprofile.site

# expose port
EXPOSE 80

# run app on container start
CMD ["R", "-e", "shiny::runApp('/srv/shiny-server/afwaapp', host = '0.0.0.0', port = 80)"]

# CMD ["/bin/bash"]


